Codigo para generar muestreos del plano complejo e iterar la funcion f(w) = w^2 + z.

sampler genera los muestreos

iterador toma z como parametro

iterador5 tiene valores hard-codeados para el rango de parametros en C que explora (pero los puedes modificar tu)

iterador8 toma como parametro dos intervalos que definen al rectangulo de parametros que se explora, y ademas utiliza una medida de la velocidad de divergencia de los puntos para representar al fractal (en vez de el angulo, como lo hacen los previos)

Para correr el codigo primero tenemos que generar un muestreo (un cuadrado compuesto de puntos en C):

>octave: a = sampler(-1,1,-1,1,1/30)

y luego utilizarlo con algun iterador (lee las definiciones para entender como :)

