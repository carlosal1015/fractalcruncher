# muestreo es generado con sampler, por lo que es un arreglo de numeros complejos representando el plano complejo inicial

# z es un parametro complejo que determina la dinamica

# n es la cantidad de veces que se aplica la funcion f(w) = w*w + z

# esta funcion regresa un arreglo que representa en cada entrada, la imagen nesima de f. O sea: f(f(...f(w)))..)



function iterr = iterador(muestreo, n, z)
  iterr=muestreo;
  for i = 1:1:n
	    for j = 1:1:size(muestreo)(1)
	      for k = 1:1:size(muestreo)(2)
		iterr(j,k) = iterr(j,k)*(iterr(j,k)+z);
	      endfor
	    endfor
  endfor

endfunction
